<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeCreationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume_creations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('interview_code');
            $table->string('designation');
            $table->string('category');
            $table->string('email_id');
            $table->string('phone_numer');
            $table->string('resume_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume_creations');
    }
}
