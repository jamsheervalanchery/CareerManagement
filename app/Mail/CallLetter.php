<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use App\Interview;

class CallLetter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
       public function build(request $request)
    {
      
       $details=Interview::where('id','=',$request->id)->first();
               
        return $this->markdown('callLetter')
                    ->with([
                        'name' => $details->name,
                        'code' => $details->interview_code,
                        'post' => $details->post,
                        'date'=> $details->date,
                        'venu' => $details->venu,
                        'rounds' => $details->rounds,
                        ])
                    ->to($details->mail_id);
    }
}