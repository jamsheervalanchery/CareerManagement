<!DOCTYPE html>
<html lang="en">
<head>
  <title>Add job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/custom.js"></script>
</head>
<body>


<div id="wrapper" class="active">  
    <!-- Sidebar -->
            <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul id="sidebar_menu" class="sidebar-nav">
           <li class="sidebar-brand"><a id="menu-toggle" href="#">Dashboard<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
        </ul>
        <ul class="sidebar-nav" id="sidebar">
          <li><a>Jobs<span class=""></span></a></li>
           <ul class="sidebar-nav" id="sidebar">
                <li><a href="">Add Job</a></li>
                <li><a href="/jobview">view Job</a></li>
           </ul>
	<li><a>Interview<span class=""></span></a></li>
           <ul class="sidebar-nav" id="sidebar">
                <li><a href="">View Candidates</a></li>
                <li><a href="">Create Interview</a></li>
		<li><a href="">Interview Status</a></li>
           </ul>
	<li><a>Resume Bank<span class=""></span></a></li>
           <ul class="sidebar-nav" id="sidebar">
                <li><a href="">View Resume</a></li>
           </ul>
          
      </div>
          
      <!-- Page content -->
      <div id="page-content-wrapper">
        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset">
            <div class="row">
              <div class="col-md-12">
              
              <div class="container">
		
                <div class="panel panel-primary col-md-6">
   
    <div class="panel-body">
      <form>
    <div class="form-group">
      <label for="usr"> Post name:</label>
      <input type="text" class="form-control" id="usr">
    </div>

  <div class="form-group">
    <label for="email">Description:</label>
    <textarea class="form-control" rows="5" id="desc"></textarea>
  </div>
  <div class="form-group">
    <label for="email">Special Notes:</label>
    <textarea class="form-control" rows="5" id="Special notes"></textarea>
  </div>
  <div class="form-group">
      <button type="button" class="btn btn-info">Save</button>
      <button type="button" class="btn btn-info">Draft</button>
    </div>

            </div>
              
            </div>
          </div>
        </div>
    </div>  
</div>
</body>
</html>