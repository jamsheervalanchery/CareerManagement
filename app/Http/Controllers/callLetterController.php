<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\CallLetter;
use DB;
use App\Interview;
use Session;
use App\jobseeker;

class callLetterController extends Controller
{
    public function send(Request $request){
	
    Mail::send(new CallLetter());
   
     $SendCallLetters = Interview::get();

    return redirect('/send_call_letter');
    }
    public function jobseekerview()
    {
        $seeker =jobseeker::get();
        return view('jobseeker')->with('seeker', $seeker);
    }
}
