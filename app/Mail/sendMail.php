<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use Illuminate\Support\Str;
use App\RegisterForm;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(request $request)
    {
        $register=RegisterForm::orderBy('id','desc')->select('verifyToken','id')->where('name',$request->name)->first();
        return $this->view('mail')
                    ->with([
                        'name' => $request->name,
                        'reg_id'=>$register->id,
                        'applied_position' => $request->applied_position,
                        'random'=> $register->verifyToken,
                        ])
                    ->to($request->to);
    }
    }


 