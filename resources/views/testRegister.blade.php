@extends('Template.app')

<div class="wrapper-form">
  <div class="panel panel-primary col-md-6 col-md-offset-3">
    <div class="panel-body">
    <form class="form-horizontal" action="{{url('TestRegister')}}" method="post">
      <div class="form-group">
        <label for="Post_name"> Post name:</label>
        <input type="text" class="form-control" name="post_name" id="usr">
      </div>

      <div class="form-group">
        <label for="Description">Description:</label>
        <textarea class="form-control" rows="5" name="desc" id="desc"></textarea>
      </div>
      <div class="form-group">
        <label for="Special_notes">Special Notes:</label>
        <textarea class="form-control" rows="5" name="special_note" id="Special notes"></textarea>
        {{csrf_field()}}
      </div>
      <div class="form-group">
        <label for="starting_date"> Starting Date:</label>
        <input type="text" class="form-control" name="starting_date" id="starting_date">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-info">Save</button>
        <button type="button" class="btn btn-info">Draft</button>
      </div>
    </div>
    </form>
  </div>
</div>
