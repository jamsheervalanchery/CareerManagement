@extends('Template.app')
@section('body')

<div class="wrapper-form">
  <div class="panel panel-primary col-md-6 col-md-offset-3">
    <div class="panel-body">

    <form class="form-horizontal" action="{{url('job')}}" method="post">
    {{csrf_field()}}
    
    @if (Session::has('message1'))
    <div class="alert alert-success">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif</div>

      <div class="form-group{{ $errors->has('post_name') ? ' has-error' : '' }}">
        <label for="Post_name"> Post name:</label>
        <input type="text" class="form-control" name="post_name" id="usr">
        @if ($errors->has('post_name'))
            <span class="help-block">
                <strong>{{ $errors->first('post_name') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('desc') ? ' has-error' : '' }}">
        <label for="Description">Description:</label>
        <textarea class="form-control" rows="5" name="desc" id="desc"></textarea>
         @if ($errors->has('desc'))
            <span class="help-block">
                <strong>{{ $errors->first('desc') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group {{ $errors->has('special_note') ? ' has-error' : '' }}">
        <label for="Special_notes">Special Notes:</label>
        <textarea class="form-control" rows="5" name="special_note" id="Special notes"></textarea>
        @if ($errors->has('speciai_note'))
            <span class="help-block">
                <strong>{{ $errors->first('special_note') }}</strong>
            </span>
        @endif
       
      </div>
      <div class="form-group {{ $errors->has('starting_date') ? ' has-error' : '' }}">
        <label for="starting_date"> Starting Date:</label>
        <input type="text" class="form-control" name="starting_date" id="datepicker">
         @if ($errors->has('starting_date'))
            <span class="help-block">
                <strong>{{ $errors->first('starting_date') }}</strong>
            </span>
        @endif
      </div>

     
      <div class=""><button type="submit" class="btn btn-info">Save</button>   
      </div>
    </div>
    </form>
  </div>
</div>
@endsection



