<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\sendMail;
use App\RegisterForm;
use Session;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Input;
class mailController extends Controller
{
    
    public function email(){
     return view('email'); 
}
    public function send(Request $request){
       $key = RegisterForm::where('name',Input::get('name'))->first();
         if ($key=='')
         {
     
            $register = new RegisterForm;
            $register->name = $request->name;
            $register->email = $request->to;
            $register->highest_qualification = $request->highest_qualification;
            $register->applied_position = $request->applied_position;
            $register->already_attended= $request->already_attended;
            $register->phone_number= $request->phone_numer;
            $register->verifyToken=Str::random(4);
         
         if($request->hasFile('resume_path','photo_path'))
         {
            $filename= $request->resume_path->getClientOriginalName();
            $request->resume_path->storeAs('public/Resume',$filename);
            $register->resume_path=$filename;
            
            $filename1= $request->photo_path->getClientOriginalName();
            $request->photo_path->storeAs('public/Photo',$filename1);
            $register->photo_path=$filename1;
            $register->save();

            Mail::send(new sendMail());
            return view('Verifyotp');
          }
        }
          else
          {
            Session::flash('message1', ' Registration and verification has failed!');
            return view('registration');
          }
          
      }
          public function verify(Request $request){

          	$otp=$request->otp;
            $data=RegisterForm::select('verifyToken')->where('id',$request->regid)->first();
            $token=$data->verifyToken;

          if($otp==$token)
          {
          	Session::flash('message1', ' Registration and verification has been Successfully done!');
          	return view('registration');
          }
          else
          {
            RegisterForm::where('id',$request->regid)->delete();
          	Session::flash('message1', ' Registration and verification has failed!');
          	return view('registration');
          }

    }
    
}



