<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::group(['middleware' => 'App\Http\Middleware\UserMiddleware'], function()
// {

Route::resource('web','RegisterController');

// });

// Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
// {

Route::resource('job','jobController');
Route::get('jobview','jobController@jobview');
Route::post('jobseeker/{id}','jobController@jobseeker');
Route::get('/delete/{id}','jobController@destroy')->name('delete');
Route::get('jobseekerview','callLetterController@jobseekerview');

Route::get('send_call_letter','interviewController@callletter');
Route::post('send_call_letter','interviewController@send_letter');
Route::get('interview_view','interviewController@viewCandidates');
Route::get('interview_status','interviewController@interview_status');
Route::get('view_details','interviewController@view_details');
Route::get('save/{id}','interviewController@saveData');
Route::get('{id}/status_show','interviewController@status_show');
Route::post('shortlist/{id}','interviewController@shortList');
Route::resource('interview','interviewController');
Route::get('show_all_status','interviewController@showAllstatus');


Route::resource('resume','ResumeController');
Route::get('viewresume','ResumeController@viewresume');
Route::get('search','ResumeController@search_resume');
Route::get('find','ResumeController@filter_resume');
Route::post('send','mailController@send');
Route::get('sendMail/{id}','callLetterController@send');

Route::get('email','mailController@email');
Route::post('verify','mailController@verify');

// });




//Route::get('view_details','RegisterController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
