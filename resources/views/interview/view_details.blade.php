<!DOCTYPE html>
<html lang="en">
<head>
  <title>Career Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<div class="panel panel-primary col-md-8 col-md-offset-3">
  <div class="panel-body">
    <form method="post" action="{{url('shortlist/'.$details->id)}}"> 
    {{csrf_field()}}   
      <table align="center" class="table table-condensed">
        <thead class="bg-primary">
          <tr>
          <th>Id</th>
            <th>Name</th>
            <th>Applied position</th>
            <th>photo</th>
            <th>Resume</th>
          </tr>
        </thead>
        <tbody>
        <td>{{$details->id}}</td>
        <td>{{$details->name}}</td>
        <td>{{$details->applied_position}}</td>
       <td><img src="<?php echo asset("storage/Photo/$details->photo_path")?>" width="100px"></img></td>
        <td><a href="{{asset('storage/Resume')}}/{{$details->resume_path}}">{{$details->resume_path}}</a></td>

        <td><button type="submit" action=""  class="btn btn-info btn-md">Add</button></td>
        </form>
        </tbody>
      </table>
    </div>
  </div>

</form>
</div></div>
</html>


