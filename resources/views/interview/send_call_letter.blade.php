
@extends('Template.app')
@section('body')
<div class="panel-group">
       <div class="panel panel-primary col-md-10 col-md-offset-2">
    <div class="panel-body">

  <table class="table table-condensed">
    <thead class="bg-primary">
      <tr>
        <th>Sl no</th>
        <th>Name</th>
        <th>Interview Code</th>
         <th>Post</th>
        <th>Date/time</th>
        <th>Venu</th>
        <th>Rounds</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <tr>
    @foreach($SendCallLetters as $SendCallLetter)    
      <td>{{ $SendCallLetter->id}} </td>
      <td>{{ $SendCallLetter->name}} </td>
      <td>{{ $SendCallLetter->interview_code}}</td>
      <td>{{ $SendCallLetter->post}}</td> 
      <td>{{ $SendCallLetter->date}}</td>
      <td>{{ $SendCallLetter->venu}}</td>
      <td>{{ $SendCallLetter->rounds}}</td>
       <td>{{ $SendCallLetter->mail_id}}</td> 
        <td>
<a href="{{url('sendMail/'.$SendCallLetter->id.'')}}"> <button type="button" class="btn btn-primary" > Send</button></a>
</td>   
    </div>
  </div>    
</tr>
@endforeach
    </tbody>
    
  </table>
  
    </div>  
    </div>
  </div>
  </div>
  </div>

@endsection
