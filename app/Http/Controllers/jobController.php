<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App\Job;
use App\jobseeker;
use Session;

class jobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('job.AddJob');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $this->validate($request, [
    'post_name' => 'required|max:255',
    'desc'=>'required',
    'special_note'=>'required',
    'starting_date'=>'required'
    ]);
    
    $job = new job;
    $job->post_name = $request->post_name;
    $job->desc = $request->desc;
    $job->special_note = $request->special_note;
    $job->starting_date= $request->starting_date;
    $job->save();
    Session::flash('message1', ' job has been Successfully Added!');
    return view('job.AddJob');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $deletejob = job::find($id);
            $deletejob->delete();
            return redirect('jobview');
    }
     public function jobview()
    {
        $jobs = Job::get();

        return view('job.jobview')->with('jobs', $jobs);
    }
    public function jobseeker(Request $request ,$id)
    {
        $job= job::find($id);
        //return $job;
        $jobseeker= new jobseeker;
        $jobseeker->post_name=$job->post_name;
        $jobseeker->description=$job->desc;
        $jobseeker->special_note=$job->special_note;
        $jobseeker->starting_date=$job->starting_date;
        $jobseeker->save();
        Session::flash('message1', ' jobs updates Successfully Added!'); 
        return redirect('jobview');
    }
     public function jobseekerview()
    {
        $seeker =jobseeker::get();
        return view('jobseekerview')->with('seeker', $seeker);
    }

}
