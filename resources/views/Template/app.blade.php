  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>Career Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  </head>
  <body>
    <div id="wrapper" class="active"> 
      <div id="sidebar-wrapper">
        <ul id="sidebar_menu" class="sidebar-nav">
            <li class="sidebar-brand"><a id="menu-toggle" href="#">Dashboard<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
        </ul>
        <ul class="sidebar-nav" id="sidebar">
              <li><a>JOBS<span class=""></span></a></li>
              <ul class="sidebar-nav" id="sidebar">
                <li><a href="/job"><center>>>add job</center></a></li>
                <li><a href="/jobview"><center>>>view job</center></a></li>
              </ul>
              <li><a>INTERVIEW<span class=""></span></a></li>
                <ul class="sidebar-nav" id="sidebar">
                  <li><a href="/interview_view"><center>View Candidates</center></a></li>
                  <li><a href="/interview"><center>Create Interview</center></a></li>
                  <li><a href="/interview_status"><center>Interview Status</center></a></li>
                  <li><a href="/show_all_status"><center> All Interview Status</center></a></li>
                  <li><a href="/send_call_letter"><center>Send call letter</center></a></li>
                 </ul>
              <li><a>RESUME BANK<span class=""></span></a></li>
                  <ul class="sidebar-nav" id="sidebar">
                  <li><a href="/resume"><center>Add Resume</center></a></li>
                  <li><a href="/viewresume"><center>View Resume</center></a></li>
                  </ul>
        </ul>
        </div>
      </div>
      <div class="container">
        <div class="row">
          @section('body')
          @show
        </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="js/custom.js"></script>
    </body>
    </html>