<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use App\Interview;
use App\RegisterForm;
use App\User;
use DB;
use Session;
use App\Interviewstatus;
use App\shortlistCandidates;
class interviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select=shortlistCandidates::get();
        return view('interview.schedule_interview')->with('select',$select);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $this->validate($request, [
    'name' => 'required|max:255',
    'interview_code'=>'required',
    'post'=>'required',
    'date'=>'required',
    'venu'=>'required',
    'rounds'=>'required',
    'mail_id'=>'required'

    
]);

    $interview = new interview;
    $interview->name = $request->name;
    $interview->interview_code = $request->interview_code;
    $interview->post = $request->post;
    $interview->date= $request->date;
    $interview->venu= $request->venu;
    $interview->rounds= $request->rounds;
    $interview->mail_id= $request->mail_id;
    $interview->save();
    Session::flash('message1', ' interview has been Successfully Added!');
    return redirect('/interview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $details = RegisterForm::find($id);
        
        return view('interview.view_details',compact('details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_details = shortlistCandidates::where('select_id',$id)->first();
        return view('interview.edit_interview_status')->with('edit_details',$edit_details);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        //return redirect('interview.interview_status');
        //return 'hai';
        $item=Interviewstatus::find($id);
        $item->attended = $request['radiotrue'];
        $item->status = $request['selection'];
        $item->comment = $request['comment'];
        $item->update();
        Session::flash('message1', ' data has been updated.');
         return redirect('interview_status');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            
    }
    public function callletter()
    {

        $SendCallLetters = Interview::get();
        return view('interview.send_call_letter')->with('SendCallLetters', $SendCallLetters);
        
    }
     public function viewCandidates()
    {
        $jobs = RegisterForm::get();
        return view('interview.applaid_candidates_view')->with('jobs', $jobs);
    }
     public function interview_status()
    {
        $jobs = Interview::get();
        return view('interview.interview_status')->with('jobs', $jobs);
    }
   
    public function saveData(Request $request, $id)
    {
       $save_id = Interview::where('id',$id)->first();
       $key=Interviewstatus::where('interview_id',$save_id->id)->count();
         if ($key==0)
         {
            $selectdata = new Interviewstatus;
            $selectdata->name=$save_id->name;
            $selectdata->interview_id=$save_id->id;
            $selectdata->interview_code=$save_id->interview_code;
            $selectdata->post=$save_id->post;
            $selectdata->date=$save_id->date;
            $selectdata->venu=$save_id->venu;
            $selectdata->attended = $request['radiotrue'];
            $selectdata->status = $request['selection'];
            $selectdata->comment = $request['comment'];
            $selectdata->save();
            Session::flash('message1', ' data has been  saved.');
            return redirect('interview_status');
        }

        else
       {
          Session::flash('message1', ' data has been already saved.');
           return redirect('interview_status');

        } 
    }
    public function shortList(Request $request ,$id)
    {
        $candidates=shortlistCandidates::where('id',$id)->count();
        if($candidates == 0)
        {
            $name=DB::table('register_forms')->where('id',$id)->value('name');
            $interview = new shortlistCandidates;
            $interview->id = $id;
            $interview->name = $name;
            $interview->save();
            Session::flash('message1', ' Candidate has been Successfully Added!');
            return redirect('/interview_view'); 
        }
        else
        {
           Session::flash('message1', ' Candidate already exits!');
            return redirect('/interview_view');   
        }    
}


 public function status_show($id)
    {
         $status = Interviewstatus::find($id);
         return view('interview.show_status_details')->with('status', $status);
        
    }
    public function showAllstatus()
    {
         $details = Interviewstatus::get(); 
        return view('interview.show_all_status')->with('details', $details);    

    }
   



   
    
}
