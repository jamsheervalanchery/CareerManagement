<?php

namespace App\Http\Middleware;

use Closure;
use User;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (@$request->user()->role != 2)
         {
            return redirect('/');
        }


        return $next($request);
    }
}
