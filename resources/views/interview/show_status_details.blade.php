
@extends('Template.app')
@section('body')
<div class="panel-group">
       <div class="panel panel-primary col-md-10 col-md-offset-2">
    <div class="panel-body">

  <table class="table table-condensed">
    <thead class="bg-primary">
      <tr>
        <th>Sl no</th>
        <th>Name</th>
        <th>Interview Code</th>
         <th>Post</th>
        <th>Date/time</th>
        <th>Venu</th>
        <th>Rounds</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
    <tr>
    @foreach($details as $details)    
      <td>{{ $details->id}} </td>
      <td>{{ $details->name}} </td>
      <td>{{ $details->interview_code}}</td>
      <td>{{ $details->post}}</td> 
      <td>{{ $details->date}}</td>
      <td>{{ $details->venu}}</td>
      <td>{{ $details->rounds}}</td>
       <td>{{ details->mail_id}}</td>    
    </div>
  </div>    
</tr>
@endforeach
    </tbody>
  </table>
  <div class="col-md-offset-4">
  <button type="submit" action="send"  class="btn btn-info btn-md">Send Call Letter</button>    
    </div>
  </div>
  </div>
  </div>

@endsection
