  <!DOCTYPE html>
<html lang="en">
<head>
  <title>Career Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
    <div class="panel panel-primary col-md-6 col-md-offset-3">
      <div class="panel-body">
       <form class="form-horizontal" method="post" action="{{route('interview.update',$edit_details->id)}}">

        {{method_field('PUT')}}
        {{csrf_field()}}
       <div class="form-group">
      <label for="name">Name:</label>
      {{$edit_details->name}}
      </div>
      <div class="form-group">
      <label for="name">Interview code:</label>
      {{$edit_details->interview_code}}
      </div>
      <div class="form-group">
      <label for="name">Post:</label>
      {{$edit_details->post}}
      </div>
      <div class="form-group">
     
      <label for="name">Date and Time:</label>
      {{$edit_details->date}}
      </div>

         <div class="form-group">
         <label for="attended">Attended</label>
          <label><input type="radio" name="radiotrue" value="attended">True @if($edit_details->attended=='attended')  @endif</label>
          
          <label><input type="radio" name="radiotrue" value="not attended" >False @if($edit_details->attended=='not attended')  @endif</label>
        </div>
<div class="form-group">
     <label for="attended">Status</label>
        <select class="selectpicker" name="selection">---select the option---
          <option value="selected">selected</option>
          <option value="selected">Not selected</option>
        </select>
</div>
      <div class="form-group">
      <label for="attended">Comment</label>
          <textarea class="form-control" rows="3" name="comment" id="comment" value="{{$edit_details->comments}}"></textarea>
      </div>
      <div class="form-group">
      <button type="submit" class="btn btn-primary">Edit</button>
      </div>
       </form>
</div>
</div>
</html>
 
 