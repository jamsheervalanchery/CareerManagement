@extends('Template.app')
@section('body')

  <div class="panel panel-primary col-md-6 col-md-offset-3">
  @if (Session::has('message1'))
          <div class="alert alert-danger">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
          @endif
    <div class="panel-body">
     <form> 
     {{csrf_field()}}         
      <table align="center" class="table table-condensed">
        <thead class="bg-primary">
          <tr>
            <th>Name</th>
            <th>Post</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach($jobs as $job)
      <td>{{ $job->name}} </td>
      <td>{{ $job->applied_position}}</td>
      <td><a href="{{url('interview/'.$job->id)}}" class="btn btn-success" role="button">view details</a></td>
    </tr>
    @endforeach

        </tbody>
      </table>
    </div>
  </div>
@endsection

