
@extends('Template.app')
@section('body')
<div class="panel-group">
       <div class="panel panel-primary col-md-10 col-md-offset-2">
    <div class="panel-body">

  <table class="table table-condensed">
    <thead class="bg-primary">
      <tr>
        <th>Sl no</th>
        <th>Name</th>
        <th>Interview Code</th>
         <th>Post</th>
        <th>Date/time</th>
        <th>Attended or not</th>
         <th>Status</th>
          <th>Comments</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
     <tr>
    @foreach($details as $details)    
      <td>{{ $details->id}} </td>
       <td>{{ $details->name}} </td>
       <td>{{ $details->interview_code}}</td>
       <td>{{ $details->post}}</td>
       <td>{{ $details->date}}</td>
       <td>{{ $details->attended}}</td>
       <td>{{ $details->status}}</td>
       <td>{{ $details->comment}}</td>

      </tr>
      @endforeach  
    </div>
    <div class="col-md-offset-4">
  <button type="submit" action="send"  class="btn btn-info btn-md">Generate pdf</button>  
  </div>
</tbody>


@endsection
