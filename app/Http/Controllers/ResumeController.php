<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App\ResumeCreation;
use Illuminate\Support\Facades\Input;
use Session;

class ResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Resume.addresume');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $this->validate($request, [
    'name' => 'required|max:255',
    'interview_code'=>'required',
    'designation'=>'required',
    'category'=>'required',
    'email_id'=>'required',
    'phone_numer'=>'required',
    'resume_path'=>'required'

    
]);
     $resume = new ResumeCreation;
     $resume->name = $request->name;
     $resume->interview_code = $request->interview_code;
     $resume->designation = $request->designation;
     $resume->category= $request->category;
     $resume->email_id= $request->email_id;
    $resume->phone_numer= $request->phone_numer;
 
         if($request->hasFile('resume_path'))
         {
            $filename= $request->resume_path->getClientOriginalName();
            $request->resume_path->storeAs('public/Resume',$filename);
             $resume->resume_path=$filename;
             $resume->save();
           //return 'uploaded';
            Session::flash('message1', ' Resume  has been Successfully Added!');
            return view('Resume.addresume');
          }
        else
        {
            Session::flash('message1', ' please upload the file!');
       return view('Resume.addresume');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function viewresume()
    {
         $viewresume = ResumeCreation::get();

        return view('Resume.viewresume')->with('viewresume ', $viewresume);
    }
    
    public function search_resume()
    {
    $search_data = $_GET['searchresult'];
    $user = ResumeCreation::where ( 'name', 'LIKE', '%' . $search_data . '%' )->orWhere ( 'email_id', 'LIKE', '%' . $search_data . '%' )->get ();
    if (count ( $user ) > 0)
        return response()->json($user);
   /* else if( count( $user ) = '')
    {
      return view ( 'Resume.viewresume' )->withMessage ( 'Try to search again !' );  
    }*/
    else
        return view ( 'Resume.viewresume' )->withMessage ( 'No Details found. Try to search again !' );
    }
    


    public function filter_resume(Request $request)
    {
      $des=$_GET['des'];
      $data = ResumeCreation::where('designation','=',$des)->get(); 
                return $data;//response()->json($data);
    }
    
}
