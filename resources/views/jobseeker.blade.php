@extends('Template.app')
@section('body')

<div class="panel panel-primary col-md-9 col-md-offset-2">
    <div class="panel-body">
       
  <table class="table table-bordered">
    <thead class="bg-primary">
      <tr>
        <th>Job Id</th>
        <th>Post name</th>
        <th>Description</th>
        <th>Special Notes</th>
        <th>Starting date</th>
      </tr>
      
    </thead>
    <tbody>
    <tr>
    @foreach($seeker as $seeker)
      <td>{{$seeker->id}}</td>
      <td>{{$seeker->post_name}} </td>
      <td>{{ $seeker->desc}}</td>
      <td>{{ $seeker->special_note}}</td> 
      <td>{{ $seeker->starting_date}}</td>
    </tr>
    @endforeach
    </tbody>
  </table>
  </div>
  </div>
</div>

@endsection
