  @extends('Template.app')
  @section('body')
    <div class="panel panel-primary col-md-6 col-md-offset-3">
      
      <div class="panel-body">
       <form action="{{url('interview')}}" method="post">
       {{csrf_field()}}

@if (Session::has('message1'))
    <div class="alert alert-success">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif</div>


       
      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name">Name</label><br>
        <select name="name" class="col-md-9" id="" >
                <option value="0" selected="selected">select</option> 
                @foreach($select as $select) 
                <option id="name" value="{{$select->name}}">{{$select->name}}
                </option> 
                  @endforeach
        </select>
        <br>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>

       <div class="form-group {{ $errors->has('interview_code') ? ' has-error' : '' }}">
        <label for="interview_code">Interview Code</label>
        <input type="text" name="interview_code" class="form-control" id="usr">
        @if ($errors->has('interview_code'))
            <span class="help-block">
                <strong>{{ $errors->first('interview_code') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('post') ? ' has-error' : '' }}">
        <label for="post">Post</label>
        <input type="text" name="post" class="form-control" id="post">
        @if ($errors->has('post'))
            <span class="help-block">
                <strong>{{ $errors->first('post') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
        <label for="usr">Date/Time</label>
        <input type="text" name="date" class="form-control" id="datepicker" >
        @if ($errors->has('date'))
            <span class="help-block">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('venu') ? ' has-error' : '' }}">
        <label for="usr">Venu</label>
        <input type="text" name="venu" class="form-control" id="usr">
        @if ($errors->has('venu'))
            <span class="help-block">
                <strong>{{ $errors->first('venu') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('rounds') ? ' has-error' : '' }}">
        <label for="usr">Rounds</label>
        <input type="text" name="rounds" class="form-control" id="usr">
         @if ($errors->has('rounds'))
            <span class="help-block">
                <strong>{{ $errors->first('rounds') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('mail_id') ? ' has-error' : '' }}">
        <label for="email-id">Email id</label>
        <input type="text" name="mail_id" class="form-control" id="usr">
        @if ($errors->has('mail_id'))
            <span class="help-block">
                <strong>{{ $errors->first('mail_id') }}</strong>
            </span>
        @endif
      </div>
      
    <div class="form-group">
        <button type="submit" class="btn btn-info">Submit</button>
      </form>
      </div>
    </div>
    
  </div>
@endsection