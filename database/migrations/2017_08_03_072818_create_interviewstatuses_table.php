<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interviewstatuses', function (Blueprint $table) {

            $table->increments('id');
            $table->string('interview_id');
            $table->String('name');
            $table->String('interview_code');
            $table->String('post');
            $table->String('date');
            $table->String('venu');
            $table->String('attended');
            $table->String('status');
            $table->String('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviewstatuses');
    }
}
