@extends('Template.app')
@section('body')

<div class="panel panel-primary col-md-12 col-md-offset-1">
    <div class="panel-body">
    
    <div class="input-group  col-md-4">
        <input type="text" class="form-control" name="search" id="search_value"
            placeholder="Search resumes" required> <span class="input-group-btn">
            <button type="button" id="searchbtn" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
    </div>  

<span>                  
    <select class="col-md-4" name="designation" id="sel">
            <option>Select</option>
            <option value='php'>Php Developer</option>
            <option value='ios'>IOS Developer</option>
            <option value='android'>Android Developer</option>
    </select> 
</span> 
    
    <table class="table table-striped " id="results" style="display: none;">
        <thead>
            <tr>
               
                <th>Name</th>
                <th>Interview code</th>
                <th>Designation</th>
                <th>E-mail</th>
                <th>Phone</th>
                <th>Resume attach</th>
            </tr>
        </thead>
        <tbody>
            
          
        </tbody>
    </table>
     
</div>
  </div>
 

@endsection
