@extends('Template.app')
@section('body')
    
 <div class="panel panel-primary col-md-10 col-md-offset-2">
 @if (Session::has('message1'))
          <div class="alert alert-danger">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
          @endif
    <div class="panel-body">
           
  <table class="table table-condensed">
    <thead class="bg-primary">
      <tr>
        <th>Sl no</th>
        <th>Name</th>
        <th>Interview Code</th>
        <th>Post</th>
        <th>Date/time</th>
        <th>Attended</th>
        <th>Status</th>
        <th>Comment</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <tr>
    @foreach($jobs as $job)
      <td>{{ $job->id}} </td>
      <td>{{ $job->name}}</td>
      <td>{{ $job->interview_code}}</td>
      <td>{{ $job->post}}</td>
      <td>{{ $job->date}}</td>
    
  <form method="put" action="{{url('save/'.$job->id)}}">
        {{csrf_field()}}
        <td><div class="radio">
          <label><input type="radio" name="radiotrue">Attended</label>
          <label><input type="radio" name="radiotrue">Not Attended</label>
        </div></td>
        <td><select class="selectpicker" name="selection">---select the option---
          <option>selected</option>
          <option>Not selected</option>
        </select>
      </td>
      <td><textarea class="form-control" rows="3" name="comment" id="comment"></textarea></td>
      <td><button type="submit" class=" btn btn-success" value="Save" name="" id="$job->id">Save</button></td>
      <td><a href="{{url('interview/'.$job->id.'/edit')}}" class="btn btn-success" role="button">Edit</a></td>
    </tr> 
  </td>
</form>
 @endforeach
</tbody>

</table>
<a href="{{url('show_all_status/')}}"> <button type="button" class="btn btn-primary" > Show all status</button></a>
</div>
</div>
@endsection