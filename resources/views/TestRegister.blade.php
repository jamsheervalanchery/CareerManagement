@extends('Template.app')

<div class="wrapper-form">
  <div class="panel panel-primary col-md-6 col-md-offset-3">
    <div class="panel-body">
    <form class="form-horizontal" action="{{url('TestRegister')}}" method="post">
      <div class="form-group">
        <label for="Post_name"> Name:</label>
        <input type="text" class="form-control" name="name" id="name">
      </div>
      <div class="form-group">
        <label for="E-mail id"> Email id</label>
        <input type="text" class="form-control" name="email_id" id="email_id">
      </div>


      
        {{csrf_field()}}
      
      
      <div class="form-group">
        <button type="submit" class="btn btn-info">Save</button>
        
      </div>
    </div>
    </form>
  </div>
</div>
