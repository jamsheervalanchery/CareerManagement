@extends('Template.app')
@section('body')
<div class="wrapper-form">
  <div class="panel panel-primary col-md-6 col-md-offset-3">
    <div class="panel-body">
    <form  action="{{url('resume')}}" enctype="multipart/form-data" method="post">
    {{csrf_field()}}
    @if (Session::has('message1'))
    <div class="alert alert-success">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif</div>

     <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name">
        
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="interview code">Interview code</label>
        <input type="text" class="form-control" name="interview_code">
         @if ($errors->has('interview_code'))
            <span class="help-block">
                <strong>{{ $errors->first('interview_code') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="Designation">Designation</label>
         <select class="form-control" name="designation">
                    <option value="php">Php developer</option> 
                    <option value="ios">Ios developer</option> 
          </select>
        
      </div>
      <div class="form-group">
        <label for="category"> category</label>
        <select class="form-control" name="category">
                    <option value="category">category</option> 
                    <option value="category">category</option> 
          </select>
      </div>
      <div class="form-group {{ $errors->has('email_id') ? ' has-error' : '' }}">
        <label for="email_id">E-mail id</label>
        <input type="text" class="form-control" name="email_id">
        @if ($errors->has('email_id'))
            <span class="help-block">
                <strong>{{ $errors->first('email_id') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group  {{ $errors->has('phone_numer') ? ' has-error' : '' }}">
        <label for="phone_number">Phone number</label>
        <input type="text" class="form-control" name="phone_numer">
         @if ($errors->has('phone_number'))
            <span class="help-block">
                <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
        @endif
        
     
    
      </div>
       <div class="form-group  {{ $errors->has('resume_path') ? ' has-error' : '' }}">
        <label for="resume_path">Resume path</label>
        <input type="file" class="form-control" name="resume_path">
        {{csrf_field()}}
         @if ($errors->has('resume_path'))
            <span class="help-block">
                <strong>{{ $errors->first('resume_path') }}</strong>
            </span>
        
        @endif

      </div>
      
      
      <div class="form-group">
        <button type="submit"  value="" class="btn btn-success col-md-offset-3">Save</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection