  @extends('Template.app')
  @section('body')
<!-- "{{url('web')}}" -->

<div class="panel-group">
    <div class="panel panel-primary col-md-6 col-md-offset-3">
      <div class="panel-body">
        <form  action="send" enctype="multipart/form-data" method="post">
          {{csrf_field()}}
          @if (Session::has('message1'))
          <div class="alert alert-success">{{ Session::get('message1') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
          @endif
        </div>



        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name">

          @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
        </div>

        <div class="form-group {{ $errors->has('email_id') ? ' has-error' : '' }}">
          <label for="email_id">E-mail id</label>
          <input type="email" class="form-control" name="to">
          @if ($errors->has('email_id'))
          <span class="help-block">
            <strong>{{ $errors->first('email_id') }}</strong>
          </span>
          @endif
        </div>
          <div class="form-group">
          <label for="highest qualification">Highest qualification</label>
          <select class="form-control" name="highest_qualification" id="qualification">
            <option value="Btech">B-tech</option> 
            <option value="Mtech">M-tech</option>
            <option value="other" id="qualification">Other</option>
          </select>
        
          <div id="text">
            <br><input type="text" class="form-control" name="highest_qualification" id="other" maxlength="30" style="display: none">
          </div>
          </div>
       

        <div class="form-group">
          <label for="applied_position"> Applied Position</label>
          <select class="form-control" name="applied_position">
            <option value="php developer">php developer</option> 
            <option value="ios developer">Ios developer</option>
            <option value="Android developer">Android developer</option>
          </select>
        </div>

        <div class="form-group {{ $errors->has('already_attended') ? ' has-error' : '' }}">
          <label for="already_attended">Already attended</label>
          <div class="radio">
            <label><input type="radio" name="yes" id="yes" value="yes">yes</label>
          </div>
          <div class="radio">
            <label><input type="radio" name="yes" id="no" value="no">no</label>

            <br><input type="text" class="form-control" placeholder="please enter the interview code" name="already_attended" id="interview_code" maxlength="30" style="display: none">
          </div>
        </div>

        <div class="form-group  {{ $errors->has('phone_numer') ? ' has-error' : '' }}">
          <label for="phone_number">Phone number</label>
          <input type="text" class="form-control" name="phone_numer">
          @if ($errors->has('phone_number'))
          <span class="help-block">
            <strong>{{ $errors->first('phone_number') }}</strong>
          </span>
          @endif



        </div>
        <div class="form-group  {{ $errors->has('photo_path') ? ' has-error' : '' }}">
          <label for="photo_path">Photo path</label>
          <input type="file" class="form-control" name="photo_path">
          {{csrf_field()}}
          @if ($errors->has('photo_path'))
          <span class="help-block">
            <strong>{{ $errors->first('photo_path') }}</strong>
          </span>

      @endif

    </div>

    <div class="form-group  {{ $errors->has('resume_path') ? ' has-error' : '' }}">
      <label for="resume_path">Resume path</label>
      <input type="file" class="form-control" name="resume_path">
      {{csrf_field()}}
      @if ($errors->has('resume_path'))
      <span class="help-block">
        <strong>{{ $errors->first('resume_path') }}</strong>
      </span>
   
  @endif

</div>


<div class="form-group">
  <button type="submit"   class="btn btn-success col-md-offset-3">Save</button>
</div>

</form>
</div>
</div>
@endsection