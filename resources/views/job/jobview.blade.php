@extends('Template.app')
@section('body')

<div class="panel panel-primary col-md-9 col-md-offset-2">
    <div class="panel-body">
       
  <table class="table table-bordered">
    <thead class="bg-primary">
      <tr>
      <th>Job Id</th>
        <th>Post name</th>
        <th>Description</th>
        <th>Special Notes</th>
        <th>Starting date</th>
         <th>Action</th>
          <th>Action</th>
      </tr>
      
    </thead>
    <tbody>
    <tr>
    @foreach($jobs as $job)
     <form method="post" action="{{url('jobseeker/'.$job->id)}}"> 
    {{csrf_field()}}   
      <td>{{$job->id}}</td>
      <td>{{$job->post_name}} </td>
      <td>{{ $job->desc}}</td>
      <td>{{ $job->special_note}}</td> 
      <td>{{ $job->starting_date}}</td>
       <td><button type="submit" class="btn btn-info btn-md">Post job</button></td>
       <td><a href="{{ route('delete', [$job->id])}}" data-method="delete" button type="button" class="btn btn-danger btn-md delete-user" name="delete">Delete</a></td>   
    </tr>
    @endforeach
    </tbody>
  </table>
 </form>
  </div>
  </div>
</div>

@endsection
