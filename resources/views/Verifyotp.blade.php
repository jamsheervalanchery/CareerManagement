
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="panel-group">
<div class="col-md-offset-4 panel panel-primary col-md-4">
<div class="panel-heading">Verify Otp</div>
	<form method="post" action="verify">
		{{csrf_field()}}
		<div class="form-group">
    <label for="email">Verify Otp:</label>
    <input type="text" class="form-control" id="otp" name="otp">
     <label for="register">Register id:</label>
    <input type="text" class="form-control" id="id" name="regid">
  </div> 
  <button type="submit" class=" col-md-offset-3 btn btn-success">Verify</button>
	</form>
</div>
</div>

</body>
</html>


